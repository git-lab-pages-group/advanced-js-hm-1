'use strickt'
// 1 
// Prototype imitation in JS allows to creat new objects based on 
// parent objeckt using parent`s prototypes.

// 2
// We call super() in child's constructor to achieve access to parent's constructor.



// Створити клас Employee, у якому будуть такі характеристики - 
// name (ім'я), age (вік), salary (зарплата). Зробіть так, 
// щоб ці характеристики заповнювалися під час створення об'єкта.

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this._salary = salary;
    }
    // Створіть гетери та сеттери для цих властивостей.
    get newName() {
        return this.name;
    }
    get newAge() {
        return this.age;
    }
    get newSalary() {
        return this.salary;
    }

    set newName(name) {
        this.name = "Ivan";
    }

    set newAge(age) {
        this.age = 22;
    }

    // set newSalary(salary) {
    //     return salary;
    // }
}

const newEmployee = new Employee('Ivan', 21, 100);
console.log(newEmployee);
 

// Створіть клас Programmer, який успадковуватиметься від класу Employee,
//  і який матиме властивість lang (список мов).
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    // Для класу Programmer перезапишіть гетер для властивості salary. 
    // Нехай він повертає властивість salary, помножену на 3.  
    get newSalary() {
        return this.salary = super.newSalary() * 3;
    }
    
}
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
 
const programer1 = new Programmer ('John', 30, 300, 'eng');
console.log(programer1);

const programer2 = new Programmer ('Ric', 20, 200, 'eng');
console.log(programer2);

 